package dummy

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class OneServiceSpec extends Specification {

    OneService oneService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new One(...).save(flush: true, failOnError: true)
        //new One(...).save(flush: true, failOnError: true)
        //One one = new One(...).save(flush: true, failOnError: true)
        //new One(...).save(flush: true, failOnError: true)
        //new One(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //one.id
    }

    void "test get"() {
        setupData()

        expect:
        oneService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ProduitDistribue> oneList = oneService.list(max: 2, offset: 2)

        then:
        oneList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        oneService.count() == 5
    }

    void "test delete"() {
        Long oneId = setupData()

        expect:
        oneService.count() == 5

        when:
        oneService.delete(oneId)
        sessionFactory.currentSession.flush()

        then:
        oneService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ProduitDistribue one = new ProduitDistribue()
        oneService.save(one)

        then:
        one.id != null
    }
}
