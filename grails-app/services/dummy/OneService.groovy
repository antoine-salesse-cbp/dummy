package dummy

import grails.gorm.services.Service

@Service(ProduitDistribue)
interface OneService {

    ProduitDistribue get(Serializable id)

    List<ProduitDistribue> list(Map args)

    Long count()

    void delete(Serializable id)

    ProduitDistribue save(ProduitDistribue one)

}