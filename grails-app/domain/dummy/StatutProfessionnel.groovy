package dummy

import groovy.transform.ToString

@ToString(includeNames = true)
class StatutProfessionnel {

    String libelle

    static belongsTo = ProduitDistribue

    static hasMany = [produitsDistribues: ProduitDistribue]

    static mapping = {
        produitsDistribues joinTable: [name: 'produit_distribue_statut_professionnel']
    }

    static constraints = {
    }
}
