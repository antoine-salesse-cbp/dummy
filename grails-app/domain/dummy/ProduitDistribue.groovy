package dummy

class ProduitDistribue {

    String libelle

    static hasMany = [statutsProfessionnels: StatutProfessionnel]

    static mapping = {
        statutsProfessionnels joinTable: [name: 'produit_distribue_statut_professionnel']
    }

    static constraints = {
    }

    enum Ids {
        MAIF(1)

        final Long id

        Ids(Long id) {
            this.id = id
        }
    }
}
