package dummy

class BootStrap {

    def init = { servletContext ->
        def produits = [
                new ProduitDistribue(libelle: 'one'),
                new ProduitDistribue(libelle: 'two'),
                new ProduitDistribue(libelle: 'three'),
        ]*.save()
        def statuts = [
                new StatutProfessionnel(libelle: 's1'),
                new StatutProfessionnel(libelle: 's2'),
                new StatutProfessionnel(libelle: 's3'),
        ]*.save()
        produits[0].addToStatutsProfessionnels(statuts[0])
        produits[0].addToStatutsProfessionnels(statuts[1])
        produits[1].addToStatutsProfessionnels(statuts[1])
        produits[2].addToStatutsProfessionnels(statuts[0])
        produits[2].addToStatutsProfessionnels(statuts[2])
    }
    def destroy = {
    }
}
