package dummy


import static org.hibernate.criterion.CriteriaSpecification.DISTINCT_ROOT_ENTITY

class OneController {

    def withCriteria() {
        def produits = ProduitDistribue.list(max: 2)
        def statuts = StatutProfessionnel.withCriteria {
            produitsDistribues {
                'in'('id', produits*.id)
            }
            readOnly(true)
            resultTransformer(DISTINCT_ROOT_ENTITY)
        }
        render statuts*.id
    }

    def findAll() {
        def produits = ProduitDistribue.list(max: 2)
        def statuts = ProduitDistribue.findAllByIdInList(produits*.id)*.statutsProfessionnels.flatten().unique()
        render statuts*.id
    }

    def find() {
        def produit = ProduitDistribue.first()
        def statuts = produit.statutsProfessionnels
        render statuts*.id
    }
}
